# Finite-difference calculation of Raman intensities in ASE (ASE-Raman)

Calculating Raman (and IR) spectroscopic data using an ASE/PySCF interface and the finite differences approach.

## Needed Python packages

1. numpy (pip3 install numpy)
2. ase (pip3 install ase)
3. pyscf (pip3 install pyscf)
4. pyscf.prop (pip3 install git+https://github.com/pyscf/properties)
5. jsonpickle (pip3 install jsonpickle)

## Installation 

1. Clone this repository:  
``
git clone https://gitlab.com/jakobkraus/ase_raman.git  
``  
2. Add this repo to your PYTHONPATH:  
``  
export PYTHONPATH=[path_to_ase_raman]/ase_raman/src/:$PYTHONPATH
``
## Use

Check out the example found in src/ase_pyscf_calculator.py.

