"""Infrared and Raman intensities"""

import os
import os.path as op
import pickle
import sys
from math import sin, pi, sqrt, log

import numpy as np

import ase.units as units
from ase.io.trajectory import Trajectory
from ase.parallel import world, paropen, parprint
from ase.utils import opencew, pickleload
from ase.calculators.singlepoint import SinglePointCalculator
from sys import stdout
from ase.io import write

def get_polarizability(atoms):
    """Calculate the electric polarizability for the atoms object.

    Only available for calculators which have a get_polarizability()
    method."""
    if atoms._calc is None:
        raise RuntimeError('Atoms object has no calculator.')
    return atoms._calc.get_polarizability(self)




class Raman:
    """Class for calculating vibrational modes, infrared and Raman intensities
    using finite difference. Based on vibrations.py and infrared.py in ASE, version 3.20.1

    The vibrational modes are calculated from a finite difference
    approximation of the Dynamical matrix, the IR intensities from
    a finite difference approximation of the gradient of the dipole
    moment and the Raman intensities from a finite difference approximation
    of the gradient of the polarizability. The method is described in:

      D. Porezag, M. R. Pederson:
      "Infrared intensities and Raman-scattering activities within
      density-functional theory",
      Phys. Rev. B 54, 7830 (1996)

    The calculator object (calc) linked to the Atoms object (atoms) must
    have the attributes:

    >>> calc.get_dipole_moment(atoms)

    for IR and

    >>> calc.get_polarizability(atoms)

    for Raman.

    Use method='Frederiksen' to use the method described in:

      T. Frederiksen, M. Paulsson, M. Brandbyge, A. P. Jauho:
      "Inelastic transport theory from first-principles: methodology
      and applications for nanoscale devices",
      Phys. Rev. B 75, 205413 (2007)

    atoms: Atoms object
        The atoms to work on.
    indices: list of int
        List of indices of atoms to vibrate.  Default behavior is
        to vibrate all atoms.
    name: str
        Name to use for files.
    delta: float
        Magnitude of displacements.
    nfree: int
        Number of displacements per degree of freedom, 2 or 4 are
        supported. Default is 2 which will displace each atom +delta
        and -delta in each cartesian direction.
    directions: list of int
        Cartesian coordinates to calculate the gradient
        of the dipole moment in.
        For example directions = 2 only dipole moment in the z-direction will
        be considered, whereas for directions = [0, 1] only the dipole
        moment in the xy-plane will be considered. Default behavior is to
        use the dipole moment in all directions.
    """

    def __init__(self, atoms, indices=None, name='raman', delta=0.01,
                 nfree=2, directions=None):
        assert nfree in [2, 4]
        self.atoms = atoms
        self.calc = atoms.calc
        if indices is None:
            indices = range(len(atoms))
        self.indices = np.asarray(indices)
        self.name = name
        self.delta = delta
        self.nfree = nfree
        self.H = None
        
        if atoms.constraints:
            print('WARNING! \n Your Atoms object is constrained. '
                  'Some forces may be unintended set to zero. \n')
        if directions is None:
            self.directions = np.asarray([0, 1, 2])
        else:
            self.directions = np.asarray(directions)
        self.ir = True
        self.ram = True
        self.atoms.get_polarizability = lambda: get_polarizability(self)
    
    def run(self):
        """Run the vibration calculations.

        This will calculate the forces for 6 displacements per atom +/-x,
        +/-y, +/-z. Only those calculations that are not already done will be
        started. Be aware that an interrupted calculation may produce an empty
        file (ending with .pckl), which must be deleted before restarting the
        job. Otherwise the forces will not be calculated for that
        displacement.

        Note that the calculations for the different displacements can be done
        simultaneously by several independent processes. This feature relies
        on the existence of files and the subsequent creation of the file in
        case it is not found.

        If the program you want to use does not have a calculator in ASE, use
        ``iterdisplace`` to get all displaced structures and calculate the forces
        on your own.
        """

        if op.isfile(self.name + '.all.pckl'):
            raise RuntimeError(
                'Cannot run calculation. ' +
                self.name + '.all.pckl must be removed or split in order ' +
                'to have only one sort of data structure at a time.')
        for dispName, atoms in self.iterdisplace(inplace=True):
            filename = dispName + '.pckl'
            fd = opencew(filename)
            if fd is not None:
                self.calculate(atoms, filename, fd)
    
    
    def iterdisplace(self, inplace=False):
        """Yield name and atoms object for initial and displaced structures.

        Use this to export the structures for each single-point calculation
        to an external program instead of using ``run()``. Then save the
        calculated gradients to <name>.pckl and continue using this instance.
        """
        atoms = self.atoms if inplace else self.atoms.copy()
        yield self.name + '.eq', atoms
        for dispName, a, i, disp in self.displacements():
            if not inplace:
                atoms = self.atoms.copy()
            pos0 = atoms.positions[a, i]
            atoms.positions[a, i] += disp
            yield dispName, atoms
            if inplace:
                atoms.positions[a, i] = pos0

    def displacements(self):
        for a in self.indices:
            for i in range(3):
                for sign in [-1, 1]:
                    for ndis in range(1, self.nfree // 2 + 1):
                        dispName = ('%s.%d%s%s' %
                                    (self.name, a, 'xyz'[i],
                                     ndis * ' +-'[sign]))
                        disp = ndis * sign * self.delta
                        yield dispName, a, i, disp


    def clean(self, empty_files=False, combined=True):
        """Remove pickle-files.

        Use empty_files=True to remove only empty files and
        combined=False to not remove the combined file.

        """

        if world.rank != 0:
            return 0

        n = 0
        filenames = [self.name + '.eq.pckl']
        if combined:
            filenames.append(self.name + '.all.pckl')
        for dispName, a, i, disp in self.displacements():
            filename = dispName + '.pckl'
            filenames.append(filename)

        for name in filenames:
            if op.isfile(name):
                if not empty_files or op.getsize(name) == 0:
                    os.remove(name)
                    n += 1
        return n

    def combine(self):
        """Combine pickle-files to one file ending with '.all.pckl'.

        The other pickle-files will be removed in order to have only one sort
        of data structure at a time.

        """
        if world.rank != 0:
            return 0
        filenames = [self.name + '.eq.pckl']
        for dispName, a, i, disp in self.displacements():
            filename = dispName + '.pckl'
            filenames.append(filename)
        combined_data = {}
        for name in filenames:
            if not op.isfile(name) or op.getsize(name) == 0:
                raise RuntimeError('Calculation is not complete. ' +
                                   name + ' is missing or empty.')
            with open(name, 'rb') as fl:
                f = pickleload(fl)
            combined_data.update({op.basename(name): f})
        filename = self.name + '.all.pckl'
        fd = opencew(filename)
        if fd is None:
            raise RuntimeError(
                'Cannot write file ' + filename +
                '. Remove old file if it exists.')
        else:
            pickle.dump(combined_data, fd, protocol=2)
            fd.close()
        return self.clean(combined=False)

    def split(self):
        """Split combined pickle-file.

        The combined pickle-file will be removed in order to have only one
        sort of data structure at a time.

        """
        if world.rank != 0:
            return 0
        combined_name = self.name + '.all.pckl'
        if not op.isfile(combined_name):
            raise RuntimeError('Cannot find combined file: ' +
                               combined_name + '.')
        with open(combined_name, 'rb') as fl:
            combined_data = pickleload(fl)
        filenames = [self.name + '.eq.pckl']
        for dispName, a, i, disp in self.displacements():
            filename = dispName + '.pckl'
            filenames.append(filename)
            if op.isfile(filename):
                raise RuntimeError(
                    'Cannot split. File ' + filename + 'already exists.')
        for name in filenames:
            fd = opencew(name)
            try:
                pickle.dump(combined_data[op.basename(name)], fd, protocol=2)
            except KeyError:
                pickle.dump(combined_data[name], fd, protocol=2)  # Old version
            fd.close()
        os.remove(combined_name)
        return 1  # One file removed


    def get_energies(self, method='standard', direction='central', **kw):
        """Get vibration energies in eV."""

        if (self.H is None or method.lower() != self.method or
            direction.lower() != self.direction):
            self.read(method, direction, **kw)
        return self.hnu

    def get_frequencies(self, method='standard', direction='central'):
        """Get vibration frequencies in cm^-1."""

        s = 1. / units.invcm
        return s * self.get_energies(method, direction)

    def get_zero_point_energy(self, freq=None):
        if freq is None:
            return 0.5 * self.hnu.real.sum()
        else:
            s = 0.01 * units._e / units._c / units._hplanck
            return 0.5 * freq.real.sum() / s

    def get_mode(self, n):
        """Get mode number ."""
        mode = np.zeros((len(self.atoms), 3))
        mode[self.indices] = (self.modes[n] * self.im).reshape((-1, 3))
        return mode


    def write_mode(self, n=None, kT=units.kB * 300, nimages=30):
        """Write mode number n to trajectory file. If n is not specified,
        writes all non-zero modes."""
        if n is None:
            for index, energy in enumerate(self.get_energies()):
                if abs(energy) > 1e-5:
                    self.write_mode(n=index, kT=kT, nimages=nimages)
            return
        mode = self.get_mode(n) * sqrt(kT / abs(self.hnu[n]))
        p = self.atoms.positions.copy()
        n %= 3 * len(self.indices)
        traj = Trajectory('%s.%d.traj' % (self.name, n), 'w')
        calc = self.atoms.calc
        self.atoms.calc = None
        for x in np.linspace(0, 2 * pi, nimages, endpoint=False):
            self.atoms.set_positions(p + sin(x) * mode)
            traj.write(self.atoms)
        self.atoms.set_positions(p)
        self.atoms.calc = calc
        traj.close()

    def show_as_force(self, n, scale=0.2):
        mode = self.get_mode(n) * len(self.hnu) * scale
        calc = SinglePointCalculator(self.atoms, forces=mode)
        self.atoms.calc = calc
        self.atoms.edit()


    def fold(self, frequencies, intensities,
             start=800.0, end=4000.0, npts=None, width=4.0,
             type='Gaussian', normalize=False):
        """Fold frequencies and intensities within the given range
        and folding method (Gaussian/Lorentzian).
        The energy unit is cm^-1.
        normalize=True ensures the integral over the peaks to give the
        intensity.
        """

        lctype = type.lower()
        assert lctype in ['gaussian', 'lorentzian']
        if not npts:
            npts = int((end - start) / width * 10 + 1)
        prefactor = 1
        if lctype == 'lorentzian':
            intensities = intensities * width * pi / 2.
            if normalize:
                prefactor = 2. / width / pi
        else:
            sigma = width / 2. / sqrt(2. * log(2.))
            if normalize:
                prefactor = 1. / sigma / sqrt(2 * pi)

        # Make array with spectrum data
        spectrum = np.empty(npts)
        energies = np.linspace(start, end, npts)
        for i, energy in enumerate(energies):
            energies[i] = energy
            if lctype == 'lorentzian':
                spectrum[i] = (intensities * 0.5 * width / pi /
                               ((frequencies - energy)**2 +
                                0.25 * width**2)).sum()
            else:
                spectrum[i] = (intensities *
                               np.exp(-(frequencies - energy)**2 /
                                      2. / sigma**2)).sum()
        return [energies, prefactor * spectrum]

    def write_dos(self, out='vib-dos.dat', start=800, end=4000,
                  npts=None, width=10,
                  type='Gaussian', method='standard', direction='central'):
        """Write out the vibrational density of states to file.

        First column is the wavenumber in cm^-1, the second column the
        folded vibrational density of states.
        Start and end points, and width of the Gaussian/Lorentzian
        should be given in cm^-1."""
        frequencies = self.get_frequencies(method, direction).real
        intensities = np.ones(len(frequencies))
        energies, spectrum = self.fold(frequencies, intensities,
                                       start, end, npts, width, type)

        # Write out spectrum in file.
        outdata = np.empty([len(energies), 2])
        outdata.T[0] = energies
        outdata.T[1] = spectrum
        fd = open(out, 'w')
        fd.write('# %s folded, width=%g cm^-1\n' % (type.title(), width))
        fd.write('# [cm^-1] arbitrary\n')
        for row in outdata:
            fd.write('%.3f  %15.5e\n' %
                     (row[0], row[1]))
        fd.close()


    def read(self, method='standard', direction='central'):
        self.method = method.lower()
        self.direction = direction.lower()
        assert self.method in ['standard', 'frederiksen']

        def load(fname, combined_data=None):
            if combined_data is not None:
                try:
                    return combined_data[op.basename(fname)]
                except KeyError:
                    return combined_data[fname]  # Old version
            return pickleload(open(fname, 'rb'))

        if direction != 'central':
            raise NotImplementedError(
                'Only central difference is implemented at the moment.')

        if op.isfile(self.name + '.all.pckl'):
            # Open the combined pickle-file
            combined_data = load(self.name + '.all.pckl')
        else:
            combined_data = None
        # Get "static" dipole moment, forces and polarizability
        name = '%s.eq.pckl' % self.name
        [forces_zero, dipole_zero, pol_zero] = load(name, combined_data)
        self.dipole_zero = (np.sum(dipole_zero**2)**0.5) / units.Debye
        self.force_zero = max([np.sum((forces_zero[j])**2)**0.5
                               for j in self.indices])
        self.pol_zero = (pol_zero[0,0]+pol_zero[1,1]+pol_zero[2,2]) / 3.0

        ndof = 3 * len(self.indices)
        H = np.empty((ndof, ndof))
        dpdx = np.empty((ndof, 3))
        dadx = np.empty((ndof, 6))
        r = 0
        for a in self.indices:
            for i in 'xyz':
                name = '%s.%d%s' % (self.name, a, i)
                [fminus, dminus, polminus] = load(name + '-.pckl', combined_data)
                [fplus, dplus, polplus] = load(name + '+.pckl', combined_data)
                # change to Voigt notation for polarizabilities
                polminus_v = np.array([polminus[0,0],polminus[1,1],polminus[2,2],polminus[1,2],polminus[0,2],polminus[0,1]])
                polplus_v = np.array([polplus[0,0],polplus[1,1],polplus[2,2],polplus[1,2],polplus[0,2],polplus[0,1]])
                if self.nfree == 4:
                    [fminusminus, dminusminus,polminusminus] = load(
                        name + '--.pckl', combined_data)
                    [fplusplus, dplusplus,polplusplus] = load(
                        name + '++.pckl', combined_data)
                    polminusminus_v = np.array([polminusminus[0,0],polminusminus[1,1],polminusminus[2,2],polminusminus[1,2],polminusminus[0,2],polminusminus[0,1]])
                    polplusplus_v = np.array([polplusplus[0,0],polplusplus[1,1],polplusplus[2,2],polplusplus[1,2],polplusplus[0,2],polplusplus[0,1]])
                if self.method == 'frederiksen':
                    fminus[a] += -fminus.sum(0)
                    fplus[a] += -fplus.sum(0)
                    if self.nfree == 4:
                        fminusminus[a] += -fminus.sum(0)
                        fplusplus[a] += -fplus.sum(0)
                if self.nfree == 2:
                    H[r] = (fminus - fplus)[self.indices].ravel() / 2.0
                    dpdx[r] = (dminus - dplus)
                    dadx[r] = (polminus_v - polplus_v)
                if self.nfree == 4:
                    H[r] = (-fminusminus + 8 * fminus - 8 * fplus +
                            fplusplus)[self.indices].ravel() / 12.0
                    dpdx[r] = (-dplusplus + 8 * dplus - 8 * dminus +
                               dminusminus) / 6.0
                    dadx[r] = (-polplusplus_v + 8 * polplus_v - 8 * polminus_v +
                               polminusminus_v) / 6.0
                H[r] /= 2 * self.delta
                dpdx[r] /= 2 * self.delta
                dadx[r] /= 2 * self.delta
                for n in range(3):
                    if n not in self.directions:
                        dpdx[r][n] = 0
                for n in range(6):
                    if n not in self.directions:
                        dadx[r][n] = 0
                r += 1
        # Calculate eigenfrequencies and eigenvectors
        m = self.atoms.get_masses()
        H += H.copy().T
        self.H = H
        m = self.atoms.get_masses()
        self.im = np.repeat(m[self.indices]**-0.5, 3)
        omega2, modes = np.linalg.eigh(self.im[:, None] * H * self.im)
        self.modes = modes.T.copy()

        # Calculate intensities
        dpdq = np.array([dpdx[j] / sqrt(m[self.indices[j // 3]] *
                                        units._amu / units._me)
                         for j in range(ndof)])
        dpdQ = np.dot(dpdq.T, modes)
        dpdQ = dpdQ.T
        
        dadq = np.array([dadx[j] / sqrt(m[self.indices[j // 3]] *
                                        units._amu / units._me)
                         for j in range(ndof)])
        dadQ = np.dot(dadq.T, modes)
        dadQ = dadQ.T
        
        # Porezag/Pederson formulae

        ir_intensities = np.array([np.sum(dpdQ[j]**2) for j in range(ndof)])
        raman_alpha = np.array([np.sum(dadQ[j,0:3]) for j in range(ndof)]) / 3.0
        raman_alpha2 = raman_alpha**2
        raman_beta2 = np.array([6 * np.sum(dadQ[j,3:]**2) + (dadQ[j,0] - dadQ[j,1])**2 + (dadQ[j,0] - dadQ[j,2])**2 + (dadQ[j,1] - dadQ[j,2])**2 for j in range(ndof)]) / 2.0
        raman_iso = 45 * raman_alpha2 
        raman_parallel = raman_iso + 4 * raman_beta2
        raman_perpendicular = 3 * raman_beta2
        raman_total = raman_parallel + raman_perpendicular
        raman_depol = (raman_perpendicular/raman_parallel) * 100

        # Conversion factor:
        s = units._hbar * 1e10 / sqrt(units._e * units._amu)
        self.hnu = s * omega2.astype(complex)**0.5
        # Conversion factor from atomic units to (D/Angstrom)^2/amu.
        conv_ir = (1.0 / units.Debye)**2 * units._amu / units._me
        self.ir_intensities = ir_intensities * conv_ir
        # Conversion factor to Angstrom^4/amu
        conv_raman = units._amu / units._me
        self.raman_iso = raman_iso * conv_raman
        self.raman_parallel = raman_parallel * conv_raman
        self.raman_perpendicular = raman_perpendicular * conv_raman
        self.raman_total = raman_total * conv_raman
        self.raman_depol = raman_depol

    def ir_intensity_prefactor(self, ir_intensity_unit):
        if ir_intensity_unit == '(D/A)^2/amu':
            return 1.0, '(D/A)^2 amu^-1'
        elif ir_intensity_unit == 'km/mol':
            # conversion factor from Porezag PRB 54 (1996) 7830
            return 42.255, 'km/mol'
        else:
            raise RuntimeError('IR intensity unit >' + ir_intensity_unit +
                               '< unknown.')

    def raman_intensity_prefactor(self, raman_intensity_unit):
        if raman_intensity_unit == 'A^4/amu':
            return 1.0, 'A^4 amu^-1'
        else:
            raise RuntimeError('Raman intensity unit >' + raman_intensity_unit +
                               '< unknown.')

    def summary(self, method='standard', direction='central',
                ir_intensity_unit='(D/A)^2/amu', raman_intensity_unit='A^4/amu',log=stdout):
        hnu = self.get_energies(method, direction)
        s = 0.01 * units._e / units._c / units._hplanck
        iu, iu_string = self.ir_intensity_prefactor(ir_intensity_unit)
        ru, ru_string = self.raman_intensity_prefactor(raman_intensity_unit)
        if ir_intensity_unit == '(D/A)^2/amu':
            iu_format = '%9.4f'
        elif ir_intensity_unit == 'km/mol':
            iu_string = '   ' + iu_string
            iu_format = ' %7.1f'
        if raman_intensity_unit == 'A^4/amu':
            ru_format = '%9.4f'
        if isinstance(log, str):
            log = paropen(log, 'a')

        parprint('---------------------------------------------------------------', file=log)
        parprint(' Mode    Frequency        IR Intensity    Total Raman Intensity', file=log)
        parprint('  #    meV     cm^-1     ' + iu_string + '          '+ ru_string, file=log)
        parprint('---------------------------------------------------------------', file=log)
        for n, e in enumerate(hnu):
            if e.imag != 0:
                c = 'i'
                e = e.imag
            else:
                c = ' '
                e = e.real
            parprint(('%3d %6.1f%s  %7.1f%s  ' + iu_format + '          ' + ru_format  ) %
                     (n, 1000 * e, c, s * e, c, iu * self.ir_intensities[n], ru * self.raman_total[n]),
                     file=log)
        parprint('---------------------------------------------------------------', file=log)
        parprint('Zero-point energy: %.3f eV' % self.get_zero_point_energy(),
                 file=log)
        parprint('Static dipole moment: %.3f D' % self.dipole_zero, file=log)
        parprint('Static isotropic polarizability: %.3f A^3' % self.pol_zero, file=log)
        parprint('Maximum force on atom in `equilibrium`: %.4f eV/A' %
                 self.force_zero, file=log)
        parprint(file=log)

    def get_spectrum(self, start=150, end=4000, npts=None, width=10,
                     type='Lorentzian', method='standard', direction='central', quantity='raman',normalize=False):
        """Get infrared or Raman spectrum.

        The method returns wavenumbers in cm^-1 with corresponding
        absolute infrared intensity or total Raman intensity.
        Start and end point, and width of the Gaussian/Lorentzian should
        be given in cm^-1.
        normalize=True ensures the integral over the peaks to give the
        intensity.
        """
        frequencies = self.get_frequencies(method, direction).real
        assert quantity in ['ir','raman']
        if quantity == 'ir':
            intensities = self.ir_intensities
        else:
            intensities = self.raman_total

        return self.fold(frequencies, intensities,
                         start, end, npts, width, type, normalize)


    def write_spectrum(self, out='raman.dat', start=150, end=4000,
                      npts=None, width=10,
                      type='Lorentzian', method='standard', direction='central',
                      quantity='raman',intensity_unit='A^4/amu', normalize=False, transmission=False):
        """Write out infrared or Raman spectrum to file.

        First column is the wavenumber in cm^-1, the second column the
        absolute infrared intensities or total Raman intensities, and
        the third column the absorbance/scattering scaled so that data runs
        from 0 to 1. Instead of absorbance, the transmission can be plotted from
        1 to 0 for IR if transmission is set to True. Start and end
        point, and width of the Gaussian/Lorentzian should be given
        in cm^-1."""
        assert quantity in ['ir','raman']
        energies, spectrum = self.get_spectrum(start, end, npts, width,
                                               type, method, direction,quantity,
                                               normalize)

        # Write out spectrum in file. First column is absolute intensities.
        # Second column is absorbance/scattering scaled so that data runs from 0 to 1
        if transmission == True and quantity == 'ir':
            spectrum2 = 1. - spectrum/spectrum.max()
        else:
            spectrum2 = spectrum / spectrum.max()
        outdata = np.empty([len(energies), 3])
        outdata.T[0] = energies
        outdata.T[1] = spectrum
        outdata.T[2] = spectrum2
        fd = open(out, 'w')
        fd.write('# %s folded, width=%g cm^-1\n' % (type.title(), width))
        if quantity == 'ir':
            iu, iu_string = self.ir_intensity_prefactor(intensity_unit)
            if normalize:
                iu_string = 'cm ' + iu_string
            fd.write('# [cm^-1] %14s\n' % ('[' + iu_string + ']'))
            for row in outdata:
                fd.write('%.3f  %15.5e  %15.5e \n' % (row[0], iu * row[1], row[2]))
        else:
            ru, ru_string = self.raman_intensity_prefactor(intensity_unit)
            if normalize:
                ru_string = 'cm ' + ru_string
            fd.write('# [cm^-1] %14s\n' % ('[' + ru_string + ']'))
            for row in outdata:
                fd.write('%.3f  %15.5e  %15.5e \n' % (row[0], ru * row[1], row[2]))
        fd.close()
    
    def write_xyz(self,n=0,nimages=30):
        """Writes files for viewing of the modes with ase gui."""
        A = []
        mode = self.get_mode(n)
        for x in np.linspace(0, 2 *pi, nimages, endpoint=False):
            a = self.atoms.copy()
            a.positions += mode * sin(x)
            A.append(a)
        write(self.name + '_{:d}.xyz'.format(n),A,plain=True)

    def calculate(self,atoms,filename,fd):
        forces = self.calc.get_forces(atoms)
        if self.ir:
            dipole = self.calc.get_dipole_moment(atoms)
        if self.ram:
            pol = self.calc.get_polarizability(atoms)
        if world.rank == 0:
            if self.ir and self.ram:
                pickle.dump([forces, dipole, pol], fd, protocol=2)
                sys.stdout.write(
                    'Writing %s, dipole moment = (%.6f %.6f %.6f)\n' %
                    (filename, dipole[0], dipole[1], dipole[2]))
                sys.stdout.write(
                    'Writing %s, isotropic polarizability = %.6f\n' %
                    (filename, (pol[0,0]+pol[1,1]+pol[2,2])/3.))
            elif self.ir:
                pickle.dump([forces, dipole], fd, protocol=2)
                sys.stdout.write(
                    'Writing %s, dipole moment = (%.6f %.6f %.6f)\n' %
                    (filename, dipole[0], dipole[1], dipole[2]))
            elif self.ram:
                pickle.dump([forces, pol], fd, protocol=2)
                sys.stdout.write(
                    'Writing %s, isotropic polarizability = %.6f\n' %
                    (filename, (pol[0,0]+pol[1,1]+pol[2,2])/3.))
            else:
                pickle.dump(forces, fd, protocol=2)
                sys.stdout.write('Writing %s\n' % filename)
            fd.close()
        sys.stdout.flush()


