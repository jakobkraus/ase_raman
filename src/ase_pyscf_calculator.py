import numpy
from ase.calculators.calculator import Calculator, all_changes
from ase.units import Ha, Bohr, Debye
from pyscf.prop.polarizability.uhf import polarizability, Polarizability
import jsonpickle


class parameters():
    def __init__(self):
        self.mode = 'dft'
    def show(self):
        print('------------------------')
        print('calculation-specific parameters')
        print('------------------------')
        for v in vars(self):
            print('{}:  {}'.format(v, getattr(self, v)))
        print('\n\n')
                                                         

def ase2pyscf(ase_atoms):
    return [[atom.symbol, atom.position] for atom in ase_atoms]


def todict(x):
    return jsonpickle.encode(x, unpicklable=False)


def init_geo(mf, atoms):
    # convert ASE structural information to PySCF information
    if atoms.pbc.any():
        cell = mf.cell.copy()
        cell.atom = ase2pyscf(atoms)
        cell.a = atoms.cell.copy()
        cell.build()
        mf.reset(cell=cell.copy())
    else:
        mol = mf.mol.copy()
        mol.atom = ase2pyscf(atoms)
        mol.build()
        mf.reset(mol=mol.copy())

class PYSCF(Calculator):
    # PySCF ASE calculator
    # by JaK
    # units:  ase         -> units [eV,Angstroem,eV/Angstroem,e*A,A**3]
    #         pyscf       -> units [Ha,Bohr,Ha/Bohr,Debye,Bohr**3]

    def __init__(self, restart=None, label='PySCF', atoms=None, directory='.', **kwargs):
        # constructor
        Calculator.__init__(self, restart=restart,label=label, atoms=atoms, directory=directory, **kwargs)
        self.initialize(**kwargs)


    def initialize(self, mf=None, p=None):
        # attach the mf and p objects to the calculator
        # add the todict functionality to enable ASE trajectories:
        # https://github.com/pyscf/pyscf/issues/624
        self.mf = mf
        self.mf.todict = lambda: todict(self.mf)
        self.p = p
        self.p.todict = lambda: todict(self.p)
        self.implemented_properties = ['energy', 'forces',
                                       'dipole', 'polarizability',
                                       'evalues', 'homo']

    def set(self, **kwargs):
        # allow for a calculator reset
        changed_parameters = Calculator.set(self, **kwargs)
        if changed_parameters:
            self.reset()
    
    def get_polarizability(self,atoms=None):
        return self.get_property('polarizability',atoms)
    
    def calculate(self,atoms=None,properties=['energy'],system_changes=all_changes):
        
        Calculator.calculate(self,atoms=atoms,properties=properties,system_changes=system_changes)
        
        init_geo(self.mf, atoms)
        self.mf.kernel()
        e = self.mf.e_tot
        if self.p.mode.lower() in ['ccsd','ccsd(t)']:
            self.mphf = self.mf.CCSD()
            self.mphf.kernel()
            e = self.mphf.e_tot
            if self.p.mode.lower() == 'ccsd(t)':
                e += self.mphf.ccsd_t()
        elif self.p.mode.lower() == 'cisd':
            self.mphf = self.mf.CISD()
            self.mphf.kernel()
            e = self.mphf.e_tot
        elif self.p.mode.lower() == 'mp2':
            self.mphf = self.mf.MP2()
            self.mphf.kernel()
            e = self.mphf.e_tot
        
        self.results['energy'] = e * Ha
        
        self.results['evalues'] = self.mf.mo_energy * Ha
        
        n_up, n_dn = self.mf.mol.nelec
        if n_up != 0 and n_dn != 0:
            e_up = numpy.sort(self.results['evalues'][0])
            e_dn = numpy.sort(self.results['evalues'][1])
            homo_up = e_up[n_up - 1]
            homo_dn = e_dn[n_dn - 1]
            self.results['homo'] = max(homo_up, homo_dn)
        elif n_up != 0:
            e_up = numpy.sort(self.results['evalues'][0])
            self.results['homo'] = e_up[n_up - 1]
        elif n_dn != 0:
            e_dn = numpy.sort(self.results['evalues'][1])
            self.results['homo'] = e_dn[n_dn - 1]
        else:
            self.results['homo'] = None

        if 'forces' in properties:
            gf = self.mf.nuc_grad_method()
            if self.p.mode.lower() in ['ccsd','ccsd(t)','cisd','mp2']:
                gf = self.mphf.nuc_grad_method()
            gf.verbose = self.mf.verbose
            if self.p.mode.lower() == 'dft':
                gf.grid_response = True
            forces = -1. * gf.kernel() * (Ha / Bohr)
            totalforces = []
            totalforces.extend(forces)
            totalforces = numpy.array(totalforces)
            self.results['forces'] = totalforces

        self.results['dipole'] = self.mf.dip_moment(verbose=self.mf.verbose) * Debye
        self.results['polarizability'] = Polarizability(self.mf).polarizability() * (Bohr**3)

if __name__ == '__main__':

    from raman import Raman
    from pyscf import gto, dft
    from ase import Atoms
    from ase.optimize import FIRE
    from ase.io.trajectory import Trajectory
    from ase.io import read, write


    # geometry optimization

    atoms = Atoms('CO', [(0, 0, 0), (0, 0, 1.1)])
    mol = gto.M(atom=ase2pyscf(atoms),basis='cc-pVDZ',spin=0,charge=0)

    mf = dft.UKS(mol)
    mf.xc = 'pbe'
    mf.grids.atom_grid = (200,590)
    mf.grids.prune = None
    mf.conv_tol = 1e-6
    mf.max_cycle = 300
    mf.verbose = 4

    p = parameters()
    p.mode = 'dft'
    p.show()

    atoms.calc = PYSCF(mf=mf,p=p)

    fmax = 1e-4*Ha/Bohr
    dyn = FIRE(atoms, logfile='N2_opt.log', trajectory='N2_opt.traj')
    dyn.run(fmax=fmax)

    f = 'N2_opt.traj'
    struct = Trajectory(f)
    write('N2_opt.xyz', struct[-1])

    # Raman spectrum on optimized geometry


    atoms = read('N2_opt.xyz')
    mol = gto.M(atom=ase2pyscf(atoms),basis='cc-pVDZ',spin=0,charge=0)

    mf = dft.UKS(mol)
    mf.xc = 'pbe'
    mf.grids.atom_grid = (200,590)
    mf.grids.prune = None
    mf.conv_tol = 1e-6
    mf.max_cycle = 300
    mf.verbose = 4

    p = parameters()
    p.mode = 'dft'
    p.show()
    atoms.calc = PYSCF(mf=mf,p=p)

    ram = Raman(atoms, delta=0.005)
    ram.clean()
    ram.run()
    ram.summary()
    ram.write_spectrum(
        out='raman.dat',
        quantity='raman',
        intensity_unit='A^4/amu')

    




